<?php

namespace TestBundle\LengowTest;

class LengowTest
{
	public function getXml($url)
	{
		$feed = file_get_contents($url);
    	$xml = simplexml_load_string($feed);
    	return $xml;
	}
}