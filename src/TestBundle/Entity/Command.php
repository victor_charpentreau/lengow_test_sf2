<?php

namespace TestBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

use Symfony\Component\Validator\Constraints as Assert;

/**
 * Command
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="TestBundle\Entity\CommandRepository")
 * @UniqueEntity("order_id")
 */
class Command
{
    /**
     * @ORM\Column(name="id", type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     * @ORM\Id
     */
    private $id;

    /**
     * @ORM\Column(name="order_id", type="guid")
     * @Assert\NotBlank()
     */
    private $order_id;

    /**
     * @ORM\Column(name="amount", type="float", nullable=true)
     * @Assert\NotBlank()
     * @Assert\Type(
     *     type="float",
     *     message="The value {{ value }} is not a valid {{ type }}."
     * )
     */
    private $amount;

    /**
     * @ORM\Column(name="shipping", type="float", nullable=true)
     * @Assert\NotBlank()
     * @Assert\Type(
     *     type="float",
     *     message="The value {{ value }} is not a valid {{ type }}."
     * )
     */
    private $shipping;

    /**
     * @ORM\Column(name="currency", type="string", nullable=true)
     * @Assert\Currency
     */
    private $currency;

    /**
     * @ORM\Column(name="purchase_date", type="string", nullable=true)
     * @Assert\Regex(
     *     pattern="/^[0-9]{4}-(0[1-9]|1[0-2])-(0[1-9]|[1-2][0-9]|3[0-1])$/",
     *     match=true,
     *     message="You must enter a date in the format : yyyy-mm-dd"
     * )
     */
    private $purchase_date;

    /**
     * @ORM\Column(name="purchase_heure", type="string", nullable=true)
     * @Assert\Regex(
     *     pattern="/^([0-9]+):([0-5][0-9]):([0-5][0-9])$/",
     *     match=true,
     *     message="You must enter a time in the format : h:i:s"
     * )
     */
    private $purchase_heure;

    /**
     * Set id
     *
     * @param integer $id
     * @return Command
     */
    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set order_id
     *
     * @param guid $orderId
     * @return Command
     */
    public function setOrderId($orderId)
    {
        $this->order_id = $orderId;

        return $this;
    }

    /**
     * Get order_id
     *
     * @return guid 
     */
    public function getOrderId()
    {
        return $this->order_id;
    }

    /**
     * Set amount
     *
     * @param float $amount
     * @return Command
     */
    public function setAmount($amount)
    {
        $this->amount = $amount;

        return $this;
    }

    /**
     * Get amount
     *
     * @return float 
     */
    public function getAmount()
    {
        return $this->amount;
    }

    /**
     * Set shipping
     *
     * @param float $shipping
     * @return Command
     */
    public function setShipping($shipping)
    {
        $this->shipping = $shipping;

        return $this;
    }

    /**
     * Get shipping
     *
     * @return float 
     */
    public function getShipping()
    {
        return $this->shipping;
    }

    /**
     * Set currency
     *
     * @param string $currency
     * @return Command
     */
    public function setCurrency($currency)
    {
        $this->currency = $currency;

        return $this;
    }

    /**
     * Get currency
     *
     * @return string 
     */
    public function getCurrency()
    {
        return $this->currency;
    }

    /**
     * Set purchase_date
     *
     * @param \DateTime $purchaseDate
     * @return Command
     */
    public function setPurchaseDate($purchaseDate)
    {
        $this->purchase_date = $purchaseDate;

        return $this;
    }

    /**
     * Get purchase_date
     *
     * @return \DateTime 
     */
    public function getPurchaseDate()
    {
        return $this->purchase_date;
    }

    /**
     * Set purchase_heure
     *
     * @param \DateTime $purchaseHeure
     * @return Command
     */
    public function setPurchaseHeure($purchaseHeure)
    {
        $this->purchase_heure = $purchaseHeure;

        return $this;
    }

    /**
     * Get purchase_heure
     *
     * @return \DateTime 
     */
    public function getPurchaseHeure()
    {
        return $this->purchase_heure;
    }
}
