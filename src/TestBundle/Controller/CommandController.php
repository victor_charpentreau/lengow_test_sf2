<?php

namespace TestBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Serializer\Serializer;
use Symfony\Component\Serializer\Encoder\XmlEncoder;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;

use TestBundle\Entity\Command;

use APY\DataGridBundle\Grid\Source\Entity;

use TestBundle\Form\Type\CommandType;

class CommandController extends Controller
{
    public function indexAction()
    {
    	$service = $this->container->get('test.lengow_test');
    	$url = $this->container->getParameter('url_orders');
    	$xml = $service->getXml($url);

        $orders = $xml->orders->order;

        $em = $this->getDoctrine()->getManager();

        foreach ($orders as $order) {
            $command = $this->getDoctrine()->getRepository("TestBundle:Command")->findOneBy(array('order_id' => $order->order_id));
            if(!$command){
                $command = new Command();
                $command->setOrderId($order->order_id);
                $command->setAmount($order->order_amount);
                $command->setShipping($order->order_shipping);
                $command->setCurrency($order->order_currency);
                $command->setPurchaseDate($order->order_purchase_date);
                $command->setPurchaseHeure($order->order_purchase_heure);
                
                $em->persist($command);
            }else{
                $command->setOrderId($order->order_id);
                $command->setAmount($order->order_amount);
                $command->setShipping($order->order_shipping);
                $command->setCurrency($order->order_currency);
                $command->setPurchaseDate($order->order_purchase_date);
                $command->setPurchaseHeure($order->order_purchase_heure);
            }
        }
        $em->flush();

        $source = new Entity('TestBundle:Command');
        $grid = $this->get('grid');
        $grid->setSource($source);

        return $grid->getGridResponse('TestBundle:Command:index.html.twig', array('grid' => $grid));
    }

    public function createAction(Request $request)
    {
        $command = new Command();
        $form = $this->createForm(new CommandType(), $command);

        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($command);
            $em->flush();
            return $this->redirect($this->generateUrl('test_homepage'));
        }

        return $this->render('TestBundle:Command:new.html.twig', array(
            'form' => $form->createView(),
        ));
    }

    public function listOrderToJSONAction()
    {
        $repository = $this->getDoctrine()->getRepository('TestBundle:Command');
        $commands = $repository->findAll();

        if (!$commands) {
            throw $this->createNotFoundException('Command(s) not found');
        }

        $encoders = array(new XmlEncoder(), new JsonEncoder());
        $normalizers = array(new ObjectNormalizer());

        $serializer = new Serializer($normalizers, $encoders);
        $jsonContent = $serializer->serialize($commands, 'json');

        return new Response($jsonContent); 
    }

    public function showOrderToJSONAction($id)
    {
    	$repository = $this->getDoctrine()->getRepository('TestBundle:Command');
        $commands = $repository->findOneBy(array('order_id' => $id));

        if (!$commands) {
            throw $this->createNotFoundException('Command not found');
        }

        $encoders = array(new XmlEncoder(), new JsonEncoder());
        $normalizers = array(new ObjectNormalizer());

        $serializer = new Serializer($normalizers, $encoders);
        $jsonContent = $serializer->serialize($commands, 'json');

        return new Response($jsonContent); 
    }

    
}
