<?php

namespace TestBundle\Form\Type;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;

class CommandType extends AbstractType
{
	public function buildForm(FormBuilderInterface $builder, array $options)
	{
		$builder->add('order_id', 'text', array(
			'label' => 'Order Id',
			'attr' => array('class' => 'form-control')
		));

		$builder->add('amount', 'number', array(
			'label' => 'Amount',
			'attr' => array('class' => 'form-control')
		));

		$builder->add('shipping', 'number', array(
			'label' => 'Shipping',
			'attr' => array('class' => 'form-control')
		));

		$builder->add('currency', 'text', array(
			'label' => 'Currency',
			'attr' => array(
				'class' => 'form-control',
				'placeholder' => '(EUR/USD/GBP)'
			)
		));

		$builder->add('purchase_date', 'text', array(
			'label' => 'Purchase date',
			'attr' => array(
				'class' => 'form-control',
				'placeholder' => 'yyyy-mm-dd',
				'style' => 'width: 120px'
			)
		));

		$builder->add('purchase_heure', 'text', array(
			'label' => 'Purchase time',
			'attr' => array(
				'class' => 'form-control',
				'placeholder' => '__:__:__',
				'style' => 'width: 120px'
			)
		));

		$builder->add('save', 'submit', array(
			'label' => 'Submit',
			'attr' => array('class' => 'btn btn-success pull-right')
		));
	}

	public function getName()
	{
		return 'command';
	}
}